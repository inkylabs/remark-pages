import { isString } from '@inkylabs/fannypack'
import { promises as fs } from 'fs'
import visit from 'unist-util-visit'

const NAME = 'pages'

async function getPages (opts) {
  if (!isString(opts.pages)) return
  const json = await fs.readFile(opts.pages, 'utf8')
  try {
    opts.pages = JSON.parse(json)
  } catch (e) {
    if (e instanceof SyntaxError) {
      return `could not parse ${opts.pages}: ${e}`
    }
    throw e
  }
}

export default (opts) => {
  opts = Object.assign({
    pages: []
  }, opts)

  return async (root, f) => {
    const err = await getPages(opts)
    if (err) {
      f.message(err, root.position, `${NAME}:file-read`)
      return
    }

    const pages = opts.pages
      .filter(p => p.file === f.history[0])

    let page = pages.shift()

    visit(root, undefined, (node, index, parent) => {
      if (
        !page ||
        page.type === 'root' ||
        (page.type !== 'tex' && page.type !== node.type) ||
        page.position.start.line !== node.position.start.line ||
        page.position.start.column !== node.position.start.column) return

      if (node.type === 'text') {
        parent.children.splice(index, 1, {
          type: 'text',
          value: node.value.substring(0, page.commonCharactersStart),
          position: {
            start: page.position.start,
            end: page.position.middle1
          }
        }, {
          type: 'pagemark',
          label: page.label,
          page: page.page,
          position: {
            start: page.position.middle2,
            end: page.position.middle2
          }
        }, {
          type: 'text',
          value: node.value.substring(page.commonCharactersStart),
          position: {
            start: page.position.middle2,
            end: page.position.end
          }
        })
        page = pages.shift()
        return index + 3
      }
      parent.children.splice(index, 0, {
        type: 'pagemark',
        label: page.label,
        page: page.page,
        position: {
          start: node.position.start,
          end: node.position.start
        }
      })
      page = pages.shift()
    })
    if (pages.length) {
      f.message(`did not place page "${pages[0].label}"`, root.position,
        `${NAME}:pages-used`)
    }
  }
}
